package nl.utwente.di.celToFahr;

public class Converter {

    public double convert(String temp) {
        try {
            int cel = Integer.parseInt(temp);
            return (cel * 1.8) + 32;
        } catch (Exception e) {
            return -459.67;
        }

    }
}
